-------
-- Parser for "parens language", based on example from Parsec page.
-- couldn't get that to work, did this. 
-- Also used https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/Parsing
-------

import Text.ParserCombinators.Parsec

data Catalan = Node [Catalan]  | Leaf deriving (Show)

node :: Parser Catalan
node = do
  char '('
  catalans  <- many parenSet
  char ')'
  return $ Node catalans

leaf :: Parser Catalan
leaf = do
  char '('
  char ')'
  return Leaf

parenSet :: Parser Catalan
parenSet = try leaf <|> node

parseme s = do
  let parsed = parse parenSet "" s
  case parsed of Left err -> do putStrLn $ "oops: " ++ (show err)
                 Right ok -> do putStrLn $ show ok
 







   


  
  











